#Dixon Test Task

#Description

We have a data about our products stored in both ElasticSearch and MySQL database. 
The data is same in both and we mainly query the ElasticSearch. 
However, from time to time we need to fetch the data directly from MySQL while we do some fancy experiments in ElasticSearch. 
We would like to access the data about a concrete product from our frontend so we can display it to customer. [Ok]

As we do have quite some traffic we also need some kind of a caching to be implemented. 
A simple filesystem cache is sufficient for now but we need to be able to switch it for something more advanced in the future just by a modification of a config file. [ok]

Business always wants to know what are the most successful products. 
Therefore we also need to track the number of requests for each product. 
We are again ok with storing the information in a plain text file for now. 
Keep in mind that we will need to change the storage in future as well and swap it for something more robust. [ok]

#Your mission dear candidate, should you decide to accept it

You should create a new controller with a method accepting id of product as a parameter. The method should return json representation of product data. [no info what framework I can use or iti is ment to be Vanilla , so using Laravel 5.x]

A basic workflow for the task:

Request with product id comes in.
If product is cached retrieve from cache. [cache - no new stuff - using Laravel prefered cache]
If product is not cached retrieve from ElasticSearch/MySQL and add to cache.
Increment the number of requests for given product.
Return product data in JSON.
A controller might look like the following:


#Issues
https://discuss.elastic.co/t/elastic-elasticsearch-docker-not-assigning-permissions-to-data-directory-on-run/65812/4