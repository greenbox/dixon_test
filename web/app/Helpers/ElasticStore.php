<?php

namespace App\Helpers;

use Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\NoNodesAvailableException;
use Elasticsearch\Common\Exceptions\InvalidArgumentException;

class ElasticStore implements Store
{

	public function productRequest($productIdent = false)
	{

		$return = false;

		try {
    		$client = ClientBuilder::create()
    							   ->setHosts(Config::get('dixon.elastic_hosts'))
    							   ->build();
			$params = [
			    'index' => Config::get('dixon.index'),
			    'type' => Config::get('dixon.tpye'),
			    'id' => $productIdent
			];

			$return = $client->get($params)['_source'];

		} catch (NoNodesAvailableException $e ) {
			//do we log this ? Do we care ?
		} catch (InvalidArgumentException $e ) {
			//do we log this ? Do we care ?
		}

		return $return;
	}

}