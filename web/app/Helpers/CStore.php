<?php

namespace App\Helpers;

use Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class CStore implements Store
{

	private $expireTime = false;

	function __construct($foo = null)
	{
		$this->expireTime = Carbon::now()->addMinutes(Config::get('dixon.cache_expire_time'));
	}

	public function increaseProductCount($productIdent = false)
	{

		$return = false;

		if ($productIdent){
	    	if (!Cache::has('pc_'.$productIdent)){
				Cache::put('pc_'.$productIdent, 1, $this->getExpireTime());
	    	}else{
				Cache::put('pc_'.$productIdent,  intval(Cache::get('pc_'.$productIdent) + 1) , $this->getExpireTime());   		
	    	}

	    	$return = true;
		}

		return $return;
	}

	public function productRequest($productIdent = false)
	{
		return json_decode(Cache::get('p_'.$productIdent), true);
	}

	public function storeProduct($productIdent , $productDetails){
		Cache::put('p_'.$productIdent, json_encode($productDetails , true), $this->getExpireTime());
	}

	private function getExpireTime(){
		return $this->expireTime;
	}

}