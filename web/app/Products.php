<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DI\Container;

class Products extends Model
{
    protected $table = 'products';


    protected $fillable = [
        'id', 'some_universal_id', 'blabla', 'created_at'
    ];

    public static function prepare($id)
    {

        $container = new Container();

        $cstore = $container->get('App\Helpers\CStore');

        $cstore->increaseProductCount($id);

        $details = $cstore->productRequest($id);

        if ($details){
        	$self = new self();
        	$self->fill($details);
        	return $self;
        }


        $elasticStore = $container->get('App\Helpers\ElasticStore');
        $productDetails = $elasticStore->productRequest($id);

        if ($details){
        	$self = new self();
        	$self->fill($details);
        	$cstore->storeProduct($id , $details);
        	return $self;
        }


		$productRequest = self::where('id' , $id);
		if ($productRequest->count() > 0){
			$self = $productRequest->first();
            $cstore->storeProduct($id , $return);
            return $self;
		}

        return false;
    }
}
