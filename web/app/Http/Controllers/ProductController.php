<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;

use App\Products;


class ProductController extends Controller
{
    /**
     * @param string $id
     * @return string
     */
    public function detail($id)
    {
    	$return = Products::prepare($id);
        return response()->json($return);
    }
}
